<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesPhaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games_phase', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('phase_id');
            $table->unsignedBigInteger('team_id_1');
            $table->unsignedBigInteger('team_id_2');
            $table->integer('team_goals_1');
            $table->integer('team_goals_2');
            $table->timestamps();

            $table->foreign('phase_id')
                ->references('id')
                ->on('phase')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games_phase');
    }
}
