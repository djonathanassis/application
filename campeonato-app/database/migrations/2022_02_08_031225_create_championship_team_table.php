<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChampionshipTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('championship_team', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('championship_id');
            $table->unsignedBigInteger('team_id');
            $table->integer('punctuation');
            $table->integer('placing')->nullable();
            $table->integer('won_games');
            $table->integer('lost_games');
            $table->integer('scored_goals');
            $table->integer('taken_goals');
            $table->string('situation', 20);
            $table->timestamp('datetime_inscription');

            $table->foreign('championship_id')
                ->references('id')
                ->on('championship')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
            $table->foreign('team_id')
                ->references('id')
                ->on('team')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('championship_team');
    }
}
