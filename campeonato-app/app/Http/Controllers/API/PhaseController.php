<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Championship;
use App\Models\GamesPhase;
use App\Models\Phase;
use App\Models\Team;
use App\Services\ChampionshipService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class PhaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function index(int $id): JsonResponse
    {
        $championship = Championship::with('teams')->find($id);

        $exists = $championship->phases()->where('situation', 'Em andamento')->exists();

        if ($exists) {
            return \response()->json(['message' => 'Error!'], ResponseAlias::HTTP_NOT_FOUND);
        }

        $phase = (new ChampionshipService())->phaseChampionship($championship);

        $teams = $championship->teams()->where('situation', 'Jogando')->get();

        $phases = Phase::query()->create([
            'championship_id' => $id,
            'name' => $phase->name,
            'situation' => $phase->situation,
            'qtde_games' => $phase->qtde_games
        ]);

        $teams = (new ChampionshipService())->generatePhase($teams->toArray(), 2);

        $data = [];
        foreach ($teams as $value) {
            $items = [
                "team_id_1" => $value[0]['id'],
                "team_id_2" => $value[1]['id'],
                "team_goals_1" => 0,
                "team_goals_2" => 0
            ];

            $data[] = new GamesPhase($items);
        }

        $phases->find($phases->getAttribute('id'));

        $result = $phases->gamesPhases()->saveMany($data);

        return \response()->json($result, ResponseAlias::HTTP_CREATED);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function run(int $id): JsonResponse
    {
        if (empty($id)) {
            return \response()->json(['message' => 'Error!'], ResponseAlias::HTTP_NOT_FOUND);
        }

        $result = (new ChampionshipService())->runMatches($id);
        return \response()->json($result, ResponseAlias::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Builder[]|Collection
     */
    public function show(int $id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
