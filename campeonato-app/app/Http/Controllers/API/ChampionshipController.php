<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreChampionshipRequest;
use App\Http\Resources\ChampionshipResource;
use App\Models\Championship;
use App\Models\Team;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ChampionshipController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $search = $request->get('search', '');

        $allChampionship = Championship::with('teams')
            ->search($search)
            ->get()->toArray();

        return (new ChampionshipResource($allChampionship))
            ->response()
            ->setStatusCode(ResponseAlias::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChampionshipRequest $request
     * @return JsonResponse
     */
    public function store(StoreChampionshipRequest $request): JsonResponse
    {
        $collection = collect($request->validated());
        $collection->put('datetime_start', now()->format('Y-m-d H:i:s'));
        $teamInChampionship = Arr::shuffle(Arr::get($collection->all(), 'teamInChampionship'));

        $team = (new Team())->query()->whereIn('id', $teamInChampionship);

        if ($team->count() === 8) {

            $championship = Championship::query()->create($collection->all());

            $position = 1;
            foreach ($teamInChampionship as $item) {

                $team = $team->find($item['id']);

                sleep(1);

                $data = [
                    'datetime_inscription' => now()->format('Y-m-d H:i:s'),
                    'situation'            => "Jogando",
                    'punctuation'          => 0,
                    'won_games'            => 0,
                    'lost_games'           => 0,
                    'scored_goals'         => 0,
                    'taken_goals'          => 0,
                ];

                $championship->find($championship->getAttribute('id'))
                    ->teams()
                    ->attach($team['id'], $data);

            }

        } else {

            return (new ChampionshipResource(['message' => 'Error!']))
                ->response()
                ->setStatusCode(ResponseAlias::HTTP_FORBIDDEN);

        }

        return (new ChampionshipResource(['message' => 'sucesso!']))
            ->response()
            ->setStatusCode(ResponseAlias::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id): Response
    {
        $query = Championship::query();

        if ($query->find($id)) {
            $query->where('id', $id)
                ->delete();
        }
        return \response(null, ResponseAlias::HTTP_NO_CONTENT);
    }

}
