<?php

namespace App\Services;

use App\Models\Phase;
use App\Models\Team;
use Illuminate\Support\Arr;
use PhpParser\JsonDecoder;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ChampionshipService
{

    public function phaseChampionship($championship): object
    {
        $phase = [];

        switch ($championship->situation) {
            case $championship->situation === "A":
                $championship->update(['situation' => 'Q']);
                $phase = (object)[
                    'qtde_games' => 4,
                    'name' => 'Quartas de final',
                    'situation' => 'Em andamento'
                ];
                break;
            case $championship->situation === 'Q':
                $championship->update(['situation' => 'S']);
                $phase = (object)[
                    'qtde_games' => 2,
                    'name' => 'Semifinal',
                    'situation' => 'Em andamento'
                ];
                break;
            case $championship->situation === 'S':
                $championship->update([
                    'situation' => 'F',
                    'datetime_end' => now()->format('Y-m-d H:i:s')
                ]);
                $phase = (object)[
                    'qtde_games' => 1,
                    'name' => 'Final',
                    'situation' => 'Em andamento'
                ];
                break;
            default:
                break;
        }

        return $phase;
    }

    public function runMatches(int $id)
    {
        try {

            $phase = Phase::query()->with('gamesPhases')->where(
                [
                    ['championship_id', '=', $id],
                    ['situation', '=', 'Em andamento']
                ])
                ->first();

            if (!isset($phase)) {
                return \response()->json(['message' => 'Error!'], ResponseAlias::HTTP_NOT_FOUND);
            }

            foreach ($phase->gamesPhases as $game) {
                $goals = $this->generateGoals();
                $game->team_goals_1 = $goals['value1'];
                $game->team_goals_2 = $goals['value2'];
            }

            $games = $phase->gamesPhases()->saveMany($phase->gamesPhases);

            $teams = $this->setGamesStatus($games, $phase);

            foreach ($teams as $team){

                foreach ($team as $value){

                    $tea = Team::query()->find($value->team_id);

                    $tea->won_games += $value->won_games;
                    $tea->lost_games += $value->lost_games;
                    $tea->scored_goals += $value->scored_goals;
                    $tea->taken_goals += $value->taken_goals;

                    $tea->update($tea->toArray());
                }
            }

            return !empty($teams);

        } catch (\Exception $exception) {
            return response()->json(false, ResponseAlias::HTTP_NOT_FOUND);
        }
    }

    public function generateGoals()
    {
        $process = new Process(['python3', '../storage/app/public/teste.py']);
        $process->mustRun();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return (new JsonDecoder())->decode($process->getOutput());
    }

    public function generatePhase(array $array, int $size): array
    {
        $collection = collect($array);
        $arr = $collection
            ->keys()
            ->shuffle()
            ->toArray();

        $items = [];
        foreach ($arr as $key) {
            $items[] = $array[$key];
        }

        $results = [];
        foreach (array_chunk($items, $size) as $chunk) {
            $results[] = $chunk;
        }

        return $results;
    }

    public function setGamesStatus($games, $phase): array
    {

        $team = new Team();
        $championships = $phase->championships->find($phase->championship_id);

        $data = [];
        foreach ($games as $game) {

            $teams = $team->teste()->whereIn('team_id', [$game->team_id_1, $game->team_id_2])->get();

            $team1 = $teams[0];
            $team2 = $teams[1];

            if ($game->team_goals_1 === $game->team_goals_2) {

                if ($team1->punctuation === $team2->punctuation) {

                    if ($team1->datetime_inscription > $team2->datetime_inscription) {

                        $team1->situation = "Eliminado";
                    } else {

                        $team2->situation = "Eliminado";
                    }
                } else {
                    if ($team1->punctuation > $team2->punctuation) {

                        $team2->situation = "Eliminado";
                    } else {

                        $team1->situation = "Eliminado";
                    }
                }
            } else {
                if ($game->team_goals_1 > $game->team_goals_2) {

                    $team1->won_games++;
                    $team2->situation = "Eliminado";
                    $team2->lost_games++;
                } else {

                    $team2->won_games++;
                    $team1->situation = "Eliminado";
                    $team1->lost_games++;
                }
            }
            $team1->punctuation += $game->team_goals_1 - $game->team_goals_2;
            $team1->scored_goals += $game->team_goals_1;
            $team1->taken_goals += $game->team_goals_2;

            $team2->punctuation += $game->team_goals_2 - $game->team_goals_1;
            $team2->scored_goals += $game->team_goals_2;
            $team2->taken_goals += $game->team_goals_1;

            $team1 = collect($team1->toArray());
            $team1->pull('team_id');

            $team2 = collect($team2->toArray());
            $team2->pull('team_id');

            $championships->teams()->updateExistingPivot($game->team_id_1, $team1->all());
            $championships->teams()->updateExistingPivot($game->team_id_2, $team2->all());

           $data[] = $teams;
        }

        $phase->update(['situation' => 'Finalizado']);

        return $data;
    }

}
