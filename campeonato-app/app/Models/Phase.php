<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Phase extends Model
{
    use HasFactory, Searchable;

    protected $table = 'phase';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'situation',
        'qtde_games',
        'championship_id'
    ];

    protected array $searchableFields = ['*'];

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function gamesPhases(): HasMany
    {
        return $this->hasMany(GamesPhase::class, 'phase_id', 'id');
    }

    public function championships(): BelongsTo
    {
        return $this->belongsTo(Championship::class, 'championship_id', 'id');
    }

}
