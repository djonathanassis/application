<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Team extends Model
{
    use HasFactory, Searchable;

    protected $table = 'team';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'won_games',
        'lost_games',
        'scored_goals',
        'taken_goals'
    ];

    protected array $searchableFields = ['*'];

    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function championships(): BelongsToMany
    {
        return $this->belongsToMany(Championship::class, 'championship_team', 'team_id', 'championship_id');
    }

    public function teste(): Builder
    {
        return $this->newQuery()
            ->select(
                'ct.team_id',
                'ct.punctuation',
                'ct.placing',
                'ct.won_games',
                'ct.lost_games',
                'ct.scored_goals',
                'ct.taken_goals',
                'ct.situation',
                'ct.datetime_inscription'
            )
            ->join(
                'championship_team as ct',
                'ct.team_id',
                '=',
                'team.id');
    }
}
