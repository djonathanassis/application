<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \DateTimeInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Championship extends Model
{
    use HasFactory, Searchable;

    protected $table = 'championship';

    protected $dates = [
        'created_at',
        'updated_at',
        'datetime_start'
    ];

    protected $fillable = [
        'name',
        'situation',
        'datetime_start',
        'datetime_end',
        'situation'
    ];

    protected array $searchableFields = ['*'];

    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function teams(): BelongsToMany
    {
        return $this->belongsToMany(Team::class, 'championship_team', 'championship_id', 'team_id')
            ->withPivot('datetime_inscription', 'situation',  'punctuation', 'placing', 'won_games', 'lost_games', 'scored_goals', 'taken_goals');

    }

    public function phases(): HasMany
    {
        return $this->hasMany(Phase::class, 'championship_id', 'id');
    }

}
