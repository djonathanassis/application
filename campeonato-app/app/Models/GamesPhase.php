<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GamesPhase extends Model
{
    use HasFactory, Searchable;

    protected $table = 'games_phase';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'team_id_1',
        'team_id_2',
        'team_goals_1',
        'team_goals_2'
    ];

    protected function serializeDate(\DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

}
