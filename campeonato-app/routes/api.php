<?php

use App\Http\Controllers\API\ChampionshipController;
use App\Http\Controllers\API\PhaseController;
use App\Http\Controllers\API\TeamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => 'api'], function () {
    Route::apiResource('team', TeamController::class);
    Route::apiResource('championship', ChampionshipController::class);
    Route::get('phase/{id}', [PhaseController::class, 'index']);
    Route::get('executarPartidas/{id}', [PhaseController::class, 'run']);
    Route::get("/teste/{id}", [PhaseController::class, 'show']);
});
