# MeuCampeonato

## Getting started

1 - Clonar o repositório:

```bash
git clone https://gitlab.com/djonathanassis/application.git
```

2 - Mude para o diretório criado:

```bash
cd campeonato-app/
```
3 - Composer Install

Execute isso no seu terminal para obter instalar Composer:

```bash
composer install
```

## Description Activity

"José Gustavo, apaixonado por futebol e tecnologia, deseja ter uma
aplicação que simule os campeonatos de futebol do seu bairro, chamada Meu
Campeonato."
